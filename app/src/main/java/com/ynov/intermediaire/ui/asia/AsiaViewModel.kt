package com.ynov.intermediaire.ui.asia

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AsiaViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Asia Fragment"
    }
    val text: LiveData<String> = _text
}