package com.ynov.intermediaire.ui.eu

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class EuViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Europe Fragment"
    }
    val text: LiveData<String> = _text
}