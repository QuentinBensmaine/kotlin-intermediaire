package com.ynov.intermediaire.ui.na

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ynov.intermediaire.R
import com.ynov.intermediaire.ui.na.NaViewModel

class NaFragment : Fragment() {

    private lateinit var naViewModel: NaViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        naViewModel =
            ViewModelProviders.of(this).get(NaViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_na, container, false)
        val textView: TextView = root.findViewById(R.id.text_na)
        naViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}