package com.ynov.intermediaire.ui.asia

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ynov.intermediaire.R
import com.ynov.intermediaire.ui.asia.AsiaViewModel

class AsiaFragment : Fragment() {

    private lateinit var asiaViewModel: AsiaViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        asiaViewModel =
            ViewModelProviders.of(this).get(AsiaViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_asia, container, false)
        val textView: TextView = root.findViewById(R.id.text_asia)
        asiaViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}