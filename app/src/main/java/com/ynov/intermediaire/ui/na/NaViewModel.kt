package com.ynov.intermediaire.ui.na

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NaViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is North-America Fragment"
    }
    val text: LiveData<String> = _text
}